
This is a toy project to program pic16f676 chips.

## Build instructions

Just use make to compile and upload

~~~ shell
make
~~~

And use make to compile compile only

~~~ shell
make compile
~~~

### Build Options

You can change the board, port, and baud rate with make options.

## Support, bugs, developent, and feedback

If you have any problem running the software, suggestion or requests, I would be happy to help.
And if you would like to support the software, see CONTRIBUTING.md.

## License

MIT License

