#ARDUINO = MiniCore:avr:8:clock=16MHz_external
#PORT = ACM0

ARDUINO = arduino:avr:nano:cpu=atmega328old
#ARDUINO = esp8266:esp8266:generic
PORT = USB0
#BAUD_RATE = 115200

BAUD_RATE = 9600
#PROGRAMMER = -P arduinoasisp

FILE_NAME = $(shell basename $(shell pwd))
export HOME=$(shell echo $$HOME/.config)

.PHONY: default compile install list serial update

default: install

compile: $(FILE_NAME).ino
	arduino-cli compile --fqbn $(ARDUINO) . $(PROGRAMMER)

install: compile
	arduino-cli upload -p /dev/tty$(PORT) --fqbn $(ARDUINO) . $(PROGRAMMER)

list:
	arduino-cli board list

list_all:
	arduino-cli board listall

serial:
	stty -F /dev/tty$(PORT) raw $(BAUD_RATE)
	cat /dev/tty$(PORT)

update: install serial
