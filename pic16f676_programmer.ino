
// Copyright (c) 2022 LGuy9

#define OFF             0b0000
#define PWR             0b0001
#define PRG             0b0010
#define DTA             0b0100
#define CLK             0b1000

#define TVHHR           1       // max
#define TPPDP           5       // min
#define THLD0           5       // min
#define TDLY1           1       // min
#define TDLY2           1       // min
#define TDLY3           80      // max
#define TERA            6000    // max

#define TVHHR           1
#define TPPDP           1
#define THLD0           1
#define TDLY1           1
#define TDLY2           1
#define TDLY3           1
#define TERA            1

#define I_DELAY         500
#define TCLK            1
#define SCALE           1
#define FINAL_ADDRESS   0x100

#define FIRST_PC        0
#define LAST_BYTE       999999
#define LAST_PC         999999
#define LAST_line       999999
#define LAST_retry      5

int address = 0;
uint16_t data = 0;
uint8_t done = 0;
int pc_counter = 0;

char *hello = "Hello World!";

const char hex[] PROGMEM =
":020000040000FA\n\
:1000000000000228F830AE000030AF008030000061\n\
:1000100000009E200000000083120000B1002F08A5\n\
:10002000B000FA30B2000030B30083120000300894\n\
:10003000B4003108B500FF30B007031CB103350828\n\
:10004000340403199A2804303207B400B501B50D01\n\
:100050003308B5073408AE003508AF008030000023\n\
:1000600000009E200000000083120000B7002F084F\n\
:10007000B6003208AE003308AF0080300000000048\n\
:100080009E200000000083120000B900B5002F0878\n\
:10009000B400B80002303207B400B501B50D330822\n\
:1000A000B5073408AE003508AF008030000000000E\n\
:1000B0009E200000000083120000B500BB002F0846\n\
:1000C000BA00B400831200003608BA003708BB003B\n\
:1000D000FF30B607031CB7033B083A040319932803\n\
:1000E0003808AE003908AF00803000000000BF20A3\n\
:1000F0000000000083120000BA00340883120313CA\n\
:100100008400831383120000351883173A08831282\n\
:100110000313800083120000B80A0319B90AB40A55\n\
:100120000319B50A6228063083120000B2070318CB\n\
:10013000B30A152800000000D2280800003A03196D\n\
:10014000A628803A0319B028AF0100342E08840095\n\
:1001500083132F1883170008AF00840A00080800D3\n\
:1001600000000000CD20AD00AE0FAF03AF0A0000CD\n\
:100170000000CD20AC002D08AF002C080800003A8C\n\
:100180000319C628803A0319CD2800342E088400AC\n\
:1001900083132F188317000808002F088A002E08E1\n\
:1001A0008200080083168501FF3083128500BC01A0\n\
:1001B000BD013D08803E7F3E031DE0282C303C02FF\n\
:1001C0000318E628BC0A0319BD0AD9288312850141\n\
:1001D000BC01BD013D08803E7F3E031DF1282C304F\n\
:1001E0003C020318D428BC0A0319BD0AEA280800F7\n\
:1001F00001340034003401343E340034013400341E\n\
:020200000034C8\n\
:02400E00F23F7F\n\
:00000001FF";

/*
int my_fgets(char *line, int size, const char *from)
{
        static int pointer = 0;
        int i;

        strncpy_P(line, hex + pointer, size);

        for (i = 0; i < size - 1; i++) {
                if (line[i] == 0) {
                        //Serial.printf("\"%s\"\n", line);
                        Serial.print("LINE: \"");
                        Serial.print(line);
                        Serial.print("\"\n");
                        return NULL;
                } else if (line[i] == '\n') {
                        break;
                }

                pointer++;
        }

        line[i + 1] = 0;
        //Serial.printf("\"%s\"\n", line);
        Serial.print("LINE: \"");
        Serial.print("line");
        Serial.print("\"\n");
        return i;
}
*/

int my_fgets(char *line, int size, const char *from)
{
        static int pointer = 0;
        int i = 0;

        for (i = 0; i < size - 1; i++) {
                line[i] = pgm_read_byte(hex + pointer + i);

                //Serial.print("CHAR: \"");
                //Serial.print(line[i]);
                //Serial.print("\"\n");

                if (line[i] == 0) {
                        pointer += strlen(line);
                        return NULL;
                }

                if (line[i] == '\n')
                        break;
        }

        line[i + 1] = 0;
        pointer += strlen(line);

        //Serial.printf("\"%s\"\n", line);
        //Serial.print("LINE: \"");
        //Serial.print(line);
        //Serial.print("\"\n");
        return i;
}

void setup()
{
        Serial.begin(9600);

        for (int i = 8; i <= 11; i++)
                pinMode(i, OUTPUT);

        PORTB = PWR;

        delay(I_DELAY);
        enter_program_mode();

        char line[201];
        unsigned int line_count = 1;
        unsigned int error = 0;

        char record_length_str[11];
        char mem_location_str1[11];
        char mem_location_str2[11];
        char command_str[11];
        char record_str[101];
        char check_sum_str[11];

        unsigned long length;
        unsigned long location1;
        unsigned long location2;
        unsigned long command;
        long check_sum;

        char temp_byte_str[11];
        unsigned long temp_sum;
        unsigned long temp_byte;
        unsigned long pc = FIRST_PC;
        unsigned long temp_pc = 0;
        int bytes_count = 0;
        int done = 0;

        const unsigned long pc_wrap1 = 0x2000;
        //const long pc_wrap2 =

        Serial.print("STARTING\n");
        while (my_fgets(line, 64, hex) != NULL) {

                if (done != 0)
                        break;

                Serial.println("RAN");
                if (line[0] != ':') {
                        //Serial.print(stderr, "line %d has no record\n", line_count);
                        Serial.print("line ");
                        Serial.print(line_count);
                        Serial.print(" has no record\n");
                        error = 1;
                        break;
                }

                strncpy(record_length_str, line + 1, 5);
                record_length_str[2] = 0;
                length = strtol(record_length_str, NULL, 16);

                strncpy(mem_location_str1, line + 3, 5);
                mem_location_str1[2] = 0;
                location1 = strtol(mem_location_str1, NULL, 16);

                strncpy(mem_location_str2, line + 5, 5);
                mem_location_str2[2] = 0;
                location2 = strtol(mem_location_str2, NULL, 16);

                strncpy(command_str, line + 7, 5);
                command_str[2] = 0;
                command = strtol(command_str, NULL, 16);

                strncpy(record_str, line + 9, 90);
                record_str[length * 2] = 0;

                strncpy(check_sum_str, line + 9 + length * 2, 5);
                check_sum_str[2] = 0;
                check_sum = strtol(check_sum_str, NULL, 16);

                temp_sum = 0;

                for (unsigned long i = 0; i < length * 2; i += 2) {
                        strncpy(temp_byte_str, record_str + i, 5);
                        temp_byte_str[2] = 0;
                        temp_byte = strtol(temp_byte_str, NULL, 16);
                        temp_sum += temp_byte;
                }

                temp_sum += length + location1 + location2 + command + check_sum;

                if ((temp_sum & 255) != 0) {
                        //Serial.print(stderr, "line %d check sum %lu does not match with actual sum: %lu\n", line_count, check_sum, temp_sum);
                        Serial.print("line ");
                        Serial.print(line_count);
                        Serial.print(" check sum ");
                        Serial.print(check_sum);
                        Serial.print(" does not match with actual sum: ");
                        Serial.print(temp_sum);
                        Serial.print("\n");
                        error = 2;
                        break;
                }

                if (command == 0) {
                        temp_pc = location1 * 256 + location2;

                        for (unsigned long i = pc; i < temp_pc; i++) {
                                if (pc < pc_wrap1) {
                                        Serial.print("pc ");
                                        Serial.print(pc);
                                        Serial.print("\n");
                                        pc++;

                                        if (pc > LAST_PC) {
                                                done = 4;
                                                break;
                                        }

                                        increment_mem_address();
                                } else {
                                        pc = 0;
                                        temp_pc -= pc_wrap1;
                                        //temp_pc = 0;
                                }
                        }

                        for (unsigned long i = 0; i < length * 2; i += 2) {
                                int ret = -1;
                                int count = 0;

                                while (count < LAST_retry && ret != temp_byte) {
                                        strncpy(temp_byte_str, record_str + i, 5);
                                        temp_byte_str[2] = 0;
                                        temp_byte = strtol(temp_byte_str, NULL, 16);
                                        //Serial.printf("pc %lu, byte: %lu\n", pc, temp_byte);
                                        Serial.print("pc ");
                                        Serial.print(pc);
                                        Serial.print(", byte: ");
                                        Serial.print(temp_byte);
                                        Serial.print("\n");

                                        //write_data_mem(temp_byte);
                                        write_program_mem(temp_byte);
                                        begin_programming();
                                        //ret = read_data_mem();
                                        ret = read_program_mem();

                                        if (ret != temp_byte) {
                                                Serial.print("A byte does not match at location ");
                                                Serial.print(pc);
                                                Serial.print(", trying again\n");
                                        } else {
                                                Serial.print("byte at location ");
                                                Serial.print(pc);
                                                Serial.print(" written successfully\n");
                                                bytes_count++;

                                                if (bytes_count > LAST_BYTE)
                                                        done = 1;
                                        }

                                        count++;
                                }

                                if (pc < pc_wrap1) {
                                        pc++;
                                        increment_mem_address();

                                        if (pc > LAST_PC)
                                                done = 2;

                                } else {
                                        pc = 0;
                                        temp_pc -= pc_wrap1;
                                        //temp_pc = 0;
                                }

                                if (done != 0)
                                        break;
                        }
                } else {
                        //Serial.printf(stderr, "other command found\n");
                        Serial.print("other command found\n");
                }

                line_count++;

                if (line_count > LAST_line)
                        done = 3;

        }
        Serial.print("STOPPING\n");

        if (error) {
                //Serial.printf(stderr, "\nstopped reading with error %d\n", error);
                Serial.print("\nstopped reading with error ");
                Serial.print(error);
        } else {
                //Serial.printf(stderr, "\nfinished reading file, no errors encountered\n");
                Serial.print("\nfinished reading file, no errors encountered\n");
        }

        /*
        for (int i = 0; i < 13; i++) {
        //for (int i = 20; i < 100; i++) {
                write_data_mem(hello[i]);
                //write_data_mem(i);
                begin_programming();
                read_data_mem();
                increment_mem_address();
        }

        for (;;) {
                read_data_mem();
                increment_mem_address();
        }
        */
}

void clock_on()
{
        PORTB |= CLK;
        //Serial.print("DTA: ");
        //Serial.print((PORTB & DTA) == 0 ? 0:1);
        //Serial.print("\tCLK: ");
        //Serial.println(!!(PORTB & CLK));
}

void clock_off()
{
        PORTB &= ~CLK;
        //Serial.print("DTA: ");
        //Serial.print(PORTB & DTA == 0 ? 0:1);
        //Serial.print("\tCLK: ");
        //Serial.println(!!(PORTB & CLK));
}

void data_on()
{
        PORTB |= DTA;
        //Serial.print("DTA: ");
        //Serial.print((PORTB & DTA) == 0 ? 0:1);
        //Serial.print("\tCLK: ");
        //Serial.println((PORTB & CLK) == 0 ? 0:1);
}

void data_off()
{
        PORTB &= ~DTA;
        //Serial.print("DTA: ");
        //Serial.print((PORTB & DTA) == 0 ? 0:1);
        //Serial.print("\tCLK: ");
        //Serial.println((PORTB & CLK) == 0 ? 0:1);
}

void power_on()
{
        PORTB &= ~PWR;
}

void power_off()
{
        PORTB |= PWR;
}

void programming_on()
{
        PORTB |= PRG;
}

void programming_off()
{
        PORTB &= ~PRG;
}

void enter_program_mode()
{
        Serial.println("STARTING");

        data_off();
        clock_off();

        delay(TPPDP);
        delay(I_DELAY);

        programming_on();
        delay(TPPDP);
        delay(I_DELAY);

        power_on();
        delay(THLD0);
        delay(I_DELAY);

        //data_off();
        //clock_off();
        data_on();
        clock_on();

        delay(TDLY2);
        Serial.println("STARTED");
        Serial.println("");
}

void pulse_clock()
{
        delay(TCLK);
        clock_on();
        delay(TCLK);
        clock_off();
        delay(TCLK);
}

void write_program_mem(uint8_t data)
{
        Serial.println("WRITE MEM");
        Serial.print("VALUE: ");
        Serial.println(data);

        data_off();
        pulse_clock();
        data_on();
        pulse_clock();
        data_off();
        pulse_clock();
        data_off();
        pulse_clock();

        data_off();
        pulse_clock();
        data_off();
        pulse_clock();

        delay(TDLY2);
        pulse_clock();

        for (int i = 0; i < 14; i++) {
                clock_on();
                delay(TCLK / 2);

                digitalWrite(10, (data >> i) & 1);

                delay(TCLK / 2);
                clock_off();

                delay(TCLK);
        }

        pulse_clock();
        delay(TDLY2);

        Serial.println();
}

int read_program_mem()
{
        Serial.println("READ MEM");

        int value = 0;

        data_off();
        pulse_clock();
        data_off();
        pulse_clock();
        data_on();
        pulse_clock();
        data_off();
        pulse_clock();

        data_off();
        pulse_clock();
        data_off();
        pulse_clock();

        delay(TDLY2);

        pinMode(10, INPUT_PULLUP);
        //pinMode(10, INPUT);
        pulse_clock();

        //Serial.println("EMPTY CLOCK");
        //pulse_clock();

        //Serial.println("INPUT");
        for (int i = 0; i < 14; i++) {
                clock_on();
                //delay(TDLY3);
                delay(TCLK / 2);

                //Serial.print("bit: ");
                //Serial.print(i);
                //Serial.println(bit);
                int bit = digitalRead(10) << i;
                value |= bit;
                //delay(TCLK - TDLY3);
                delay(TCLK / 2);
                clock_off();
                delay(TCLK);
        }

        pulse_clock();
        delay(TDLY2);

        pinMode(10, OUTPUT);

        Serial.print("VALUE: ");
        Serial.println(value);

        //Serial.println("EMPTY CLOCK");
        //pulse_clock();

        Serial.println();
        return value;
}

void write_data_mem(uint8_t data)
{
        Serial.println("WRITE DATA");
        Serial.print("VALUE: ");
        Serial.println(data);

        data_on();
        pulse_clock();
        data_on();
        pulse_clock();
        data_off();
        pulse_clock();
        data_off();
        pulse_clock();

        data_off();
        pulse_clock();
        data_off();
        pulse_clock();

        delay(TDLY2);
        pulse_clock();

        for (int i = 0; i < 14; i++) {
                int bit = (data >> i) & 1;
                digitalWrite(10, bit);

                clock_on();
                delay(TCLK / 2);

                //Serial.print("bit: ");
                //Serial.print(i);
                //Serial.print(" ");
                //Serial.println(bit);

                delay(TCLK / 2);
                clock_off();

                delay(TCLK);
        }

        pulse_clock();
        delay(TDLY2);

        Serial.println();
}

int read_data_mem()
{
        Serial.println("READ DATA");

        int value = 0;

        data_on();
        pulse_clock();
        data_off();
        pulse_clock();
        data_on();
        pulse_clock();
        data_off();
        pulse_clock();

        data_off();
        pulse_clock();
        data_off();
        pulse_clock();

        delay(TDLY2);

        pinMode(10, INPUT_PULLUP);
        //pinMode(10, INPUT);
        pulse_clock();

        //Serial.println("EMPTY CLOCK");
        //pulse_clock();

        //Serial.println("INPUT:");
        for (int i = 0; i < 14; i++) {
                clock_on();
                //delay(TDLY3);
                delay(TCLK / 2);

                //int bit = digitalRead(10) << i;
                //Serial.print("bit: ");
                //Serial.print(i);
                //Serial.print(" ");
                //Serial.println(bit);

                //delay(TCLK - TDLY3);
                delay(TCLK / 2);

                int bit = digitalRead(10) << i;
                value |= bit;

                clock_off();
                delay(TCLK);
        }

        pulse_clock();
        delay(TDLY2);
        pinMode(10, OUTPUT);

        Serial.print("VALUE: ");
        Serial.println(value);

        //Serial.println("EMPTY CLOCK");
        //pulse_clock();

        Serial.println();
        return value;
}

void increment_mem_address()
{
        Serial.print("INC: ");
        Serial.println(pc_counter);
        pc_counter++;

        data_off();
        pulse_clock();
        data_on();
        pulse_clock();
        data_on();
        pulse_clock();
        data_off();
        pulse_clock();

        data_off();
        pulse_clock();
        data_off();
        pulse_clock();

        delay(TDLY2);

        Serial.println();
}

void begin_programming()
{
        data_off();
        pulse_clock();
        data_off();
        pulse_clock();
        data_off();
        pulse_clock();
        data_on();
        pulse_clock();
        data_on();
        pulse_clock();
        data_off();
        pulse_clock();

        //pulse_clock();

        delay(TDLY2);

        // end programming
        data_off();
        pulse_clock();
        data_on();
        pulse_clock();
        data_off();
        pulse_clock();
        data_on();
        pulse_clock();
        data_off();
        pulse_clock();
        data_off();
        pulse_clock();

        //pulse_clock();

        delay(TDLY2);
}

void loop()
{
}
